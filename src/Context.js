import React from "react";

const contextDefaults = {};

export const MapContext = React.createContext(contextDefaults);
