import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";

const mapRoot = document.getElementById("mapRoot");
const dataset = mapRoot.dataset || {};

ReactDOM.render(<App {...dataset} />, mapRoot);
