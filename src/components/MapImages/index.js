export { default as map_default } from "./map-default.png";
export { default as map_as } from "./map-as.png";
export { default as map_nu } from "./map-au.png";
export { default as map_eu } from "./map-eu.png";
export { default as map_me } from "./map-me.png";
export { default as map_us } from "./map-us.png";
