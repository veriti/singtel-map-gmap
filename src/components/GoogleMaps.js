import React, { Component } from "react";
import { MapContext } from "../Context";
import Map from "./GoogleMapsMap.js";
import Markers from "./GoogleMapsMarkers.js";
import InfoWindow from "./GoogleMapsInfoWindow.js";
import Filters from "./GoogleMapsFilters.js";
import ListView from "./GoogleMapsListView.js";

import "./GoogleMaps.css";

class GoogleMaps extends Component {
  state = {
    maps: null,
    map: null,
    sourceData: null,
    cityId: null,
    regionId: null
  };

  componentDidMount() {
    const { apiKey, sourceData, globalRef } = this.props;

    window[globalRef] = {};

    window.__onGoogleMapsAPILoaded = () => {
      this.setState({ maps: window.google.maps });
      return window.google.maps;
    };

    // load google maps js api
    const script = document.createElement("script");
    script.async = true;
    script.defer = true;
    script.src = `https://maps.googleapis.com/maps/api/js?key=${apiKey}&callback=__onGoogleMapsAPILoaded`;
    document.querySelector("body").appendChild(script);

    // fetch marker data
    fetch(sourceData || "https://api.myjson.com/bins/r58k2")
      .then(response => response.json())
      .then(json => this.setState({ sourceData: json }));
  }

  render() {
    const providerValues = {
      addonsPath: this.props.addonsPath,
      globalRef: this.props.globalRef,
      state: this.state,
      setState: state => this.setState(state)
    };

    return (
      <MapContext.Provider value={providerValues}>
        <MapContextListView />
        <MapContextFilters />
        <div className="smap__maps">
          <MapContextMap />
          <MapContextMarkers />
          <MapContextInfoWindow />
        </div>
      </MapContext.Provider>
    );
  }
}

const MapContextListView = () => {
  return (
    <MapContext.Consumer>
      {context => {
        const { sourceData } = context.state;
        const { globalRef } = context;

        if (!sourceData) return false;

        const props = { sourceData, globalRef };

        return <ListView {...props} />;
      }}
    </MapContext.Consumer>
  );
};

const MapContextMap = () => (
  <MapContext.Consumer>
    {context => {
      const { maps } = context.state;

      if (!maps) return null;

      const props = {
        maps,
        onMapInit: map => context.setState({ map: map })
      };

      return <Map {...props} />;
    }}
  </MapContext.Consumer>
);

const MapContextMarkers = () => (
  <MapContext.Consumer>
    {context => {
      const { maps, map, sourceData, regionId, cityId } = context.state;

      if (!maps || !map || !sourceData) return null;

      const props = {
        maps,
        map,
        addonsPath: context.addonsPath,
        sourceData,
        regionId,
        cityId,
        onMarkerClick: id => context.setState({ cityId: id })
      };

      return <Markers {...props} />;
    }}
  </MapContext.Consumer>
);

const MapContextFilters = () => (
  <MapContext.Consumer>
    {context => {
      const { sourceData, regionId, cityId } = context.state;
      const props = {
        sourceData,
        regionId,
        cityId,
        onFilter: rid => context.setState({ regionId: rid, cityId: null }),
        onOfficeSelect: cid => context.setState({ regionId: null, cityId: cid })
      };

      return <Filters {...props} />;
    }}
  </MapContext.Consumer>
);

const MapContextInfoWindow = () => (
  <MapContext.Consumer>
    {context => {
      const { map, sourceData, regionId, cityId } = context.state;
      const { globalRef } = context;

      if (!sourceData) return null;

      const props = {
        map,
        sourceData,
        regionId,
        cityId,
        globalRef,
        onClose: () => context.setState({ regionId: null, cityId: null })
      };

      return <InfoWindow {...props} />;
    }}
  </MapContext.Consumer>
);

export default GoogleMaps;
