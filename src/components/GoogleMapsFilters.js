import React, { Component } from "react";

class Fiter extends Component {
  handleFilterClick = rid => {
    const { onFilter } = this.props;
    onFilter(rid);
  };

  handleDropDownChange = event => {
    const { onOfficeSelect } = this.props;
    const value = !event.target.value ? null : event.target.value;
    onOfficeSelect(value);
  };

  render() {
    const { sourceData, regionId, cityId } = this.props;

    if (!sourceData) return null;

    const regions = sourceData.cities.reduce((a, city) => {
      if (a.includes(city.regionId)) return a;
      return a.concat([city.regionId]);
    }, []);

    return (
      <div className="smap__controls">
        <select
          className="smap__dropdown"
          onChange={this.handleDropDownChange}
          value={cityId ? cityId : ""}
        >
          <option value="">Select an office</option>
          {sourceData.offices.map((office, i) => {
            return (
              <option key={i} value={office.cityId}>
                {office.name}
              </option>
            );
          })}
        </select>

        <div className="smap__filters">
          <button
            className={`smap__filter${
              ["NOA"].includes(regionId) ? ` smap__filter--active` : ``
            }`}
            onClick={() => this.handleFilterClick("NOA")}
            disabled={!regions.includes("NOA")}
          >
            Americas
          </button>

          <button
            className={`smap__filter${
              ["SEA"].includes(regionId) ? ` smap__filter--active` : ``
            }`}
            onClick={() => this.handleFilterClick("SEA")}
            disabled={!regions.includes("SEA")}
          >
            Asia
          </button>

          <button
            className={`smap__filter${
              ["AUS"].includes(regionId) ? ` smap__filter--active` : ``
            }`}
            onClick={() => this.handleFilterClick("AUS")}
            disabled={!regions.includes("AUS")}
          >
            Australia
          </button>

          <button
            className={`smap__filter${
              ["EUR"].includes(regionId) ? ` smap__filter--active` : ``
            }`}
            onClick={() => this.handleFilterClick("EUR")}
            disabled={!regions.includes("EUR")}
          >
            Europe
          </button>

          <button
            className={`smap__filter${
              ["MEA"].includes(regionId) ? ` smap__filter--active` : ``
            }`}
            onClick={() => this.handleFilterClick("MEA")}
            disabled={!regions.includes("MEA")}
          >
            Middle East
          </button>
        </div>
      </div>
    );
  }
}

export default Fiter;
