import React, { Component } from "react";
import mapOptions from "./GoogleMapsOptions.json";

class Map extends Component {
  constructor(props) {
    super(props);
    this.mapDOM = React.createRef();
  }

  state = {
    initialized: false
  }

  componentDidMount() {
    const { maps, onMapInit } = this.props;
    const map = new maps.Map(this.mapDOM.current, mapOptions);

    maps.event.addListenerOnce(map, "idle", () => {
      this.setState({ initialized: true })
      onMapInit(map)
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.initialized) return false
    return true
  }

  render() {
    return <div className="smap__gmap" ref={this.mapDOM} />;
  }
}

export default Map;
