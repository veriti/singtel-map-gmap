import { Component } from "react";
import { IconCluster, IconMarker } from "./MapIcons"

class Markers extends Component {
  constructor(props) {
    super(props);
    this.clusterer = null;
  }

  state = {
    initialized: false,
    cities: []
  };

  loadMarkerClustererLib = () => {
    const { addonsPath } = this.props
    const script = document.createElement("script");

    document.querySelector("body").appendChild(script);
    script.onload = () => this.initClusterer()
    script.asyn = true;
    script.src = addonsPath ? addonsPath : 'build/google-maps-addons.js';
  };

  initClusterer = () => {
    const { maps, map } = this.props
    this.clusterer = new window.MarkerClusterer(map, [], {
      ignoreHidden: false,
      maxZoom: 7,
      styles: [{
        url: IconCluster,
        width: 40,
        height: 56,
        anchorIcon: [56, 20],
        anchorText: [-8, 0],
        textSize: 14
      }]
    })
    maps.event.addListenerOnce(this.clusterer, "clusteringend", () => {
      this.setState({ initialized: true })
    })
  }

  handleMarkerClick = (id, event) => {
    const { onMarkerClick } = this.props
    onMarkerClick(id)
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { sourceData, regionId } = nextProps

    const cities = regionId
      ? sourceData.cities.filter(city => city.regionId === regionId)
      : sourceData.cities

    return {
      cities: cities
    }
  }

  shouldComponentUpdate(nextProps) {
    if (nextProps.cityId !== null) return false
    return true
  }

  componentDidMount() {
    this.loadMarkerClustererLib();
  }

  render() {
    const { maps, map } = this.props;
    const { initialized, cities } = this.state;

    if (!initialized) return null

    if (this.clusterer.getMarkers().length) {
      this.clusterer.clearMarkers()
    }

    const markers = cities.map(city => {
      const { lat, lng, id } = city
      const marker = new maps.Marker({
        position: { lat: +lat, lng: +lng },
        icon: IconMarker
      })

      maps.event.addListener(marker, "click", this.handleMarkerClick.bind(this, id))

      return marker
    })

    this.clusterer.addMarkers(markers)

    const newBounds = this.clusterer.getMarkers().reduce((a, marker) => {
        return a.extend(marker.getPosition())
      }, new maps.LatLngBounds())

    map.fitBounds(newBounds, 80)

    return null
  }
}

export default Markers;
