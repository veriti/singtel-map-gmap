import React, { Component } from "react";

class ListView extends Component {
  state = {
    activeIndex: -1,
    regions: []
  };

  toggleIndex = index => {
    const { activeIndex } = this.state;
    const newIndex = index === activeIndex ? -1 : index;
    this.setState({ activeIndex: newIndex });
  };

  static getDerivedStateFromProps(nextProps) {
    const { sourceData } = nextProps;
    if (sourceData) {
      const regions = sourceData.cities.reduce((a, city) => {
        const searchIndex = a.findIndex(region => region.id === city.regionId);
        const index = searchIndex < 0 ? a.length : searchIndex;
        a[index] = a[index] || {};
        a[index].id = a[index].id || city.regionId;
        a[index].cities = a[index].cities || [];
        a[index].cities.push(city);
        return a;
      }, []);

      return { regions };
    }

    return null;
  }

  render() {
    const { regions, activeIndex } = this.state;
    const { sourceData: {offices}, globalRef } = this.props;

    if (!regions.length) return null;

    const regionNamesMap = [
      { regionName: "Asia", regionIds: ["SEA"] },
      { regionName: "Europe", regionIds: ["EUR"] },
      { regionName: "Americas", regionIds: ["NOA"] }
    ];

    return (
      <div className="smap__listview">
        {regions.map((region, rid) => {
          const regionName = regionNamesMap.find(map =>
            map.regionIds.includes(region.id)
          ).regionName;

          return (
            <div
              className={`lv__set${
                activeIndex === rid ? ` lv__set--active` : ``
              }`}
              key={region.id}
            >
              <button
                className="lv__set__toggle"
                onClick={() => this.toggleIndex(rid)}
              >
                {regionName}
              </button>
              {region.cities.map(city => (
                <Cards key={city.id} offices={offices} city={city} globalRef={globalRef} />
              ))}
            </div>
          );
        })}
      </div>
    );
  }
}

class Cards extends Component {
  render() {
    const { offices, city, globalRef } = this.props;

    return (
      <div className="lv__cards" key={city.id}>
        <h4 className="lv__card-head">{city.name}</h4>
        <Card key={city.id} offices={offices} city={city} globalRef={globalRef} />
      </div>
    );
  }
}

class Card extends Component {

  onContact = office => {
    const { globalRef } = this.props
    const ref = window[globalRef]

    if (!ref.onContactUsClick || typeof(ref.onContactUsClick) !== 'function') {
      alert(`${globalRef}.onContactUsClick" is not bound to any action! Assign it a function that accepts an "Object" as parameters`)
      return;
    }

    ref.onContactUsClick()
  }

  render() {
    const { offices, city } = this.props;
    const cityOffices = offices.filter(office => office.cityId === city.id);

    return cityOffices.map((office, i) => (
      <div className="lv__card" key={i}>
        <p>
          <strong>{office.name}</strong>
        </p>
        <p>
          {office.address1}
          <br />
          {office.address2}
        </p>
        <dl className="def-inline">
          <dt>Tel:</dt>
          <dd>{office.tel}</dd>
          <dt>Fax:</dt>
          <dd>{office.fax}</dd>
        </dl>
        <p>
          <button className="ux-anchor ux-button button round" onClick={() => this.onContact(office)}>Contact us</button>
        </p>
      </div>
    ));
  }
}

export default ListView;
