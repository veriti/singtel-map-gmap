import React, { Component } from "react";

class InfoWindow extends Component {
  state = {
    offices: [],
    cityName: null
  };

  static getDerivedStateFromProps(nextProps) {
    const { sourceData: { offices, cities }, cityId } = nextProps;
    const city = cities.find(city => city.id === cityId);

    return {
      offices: offices.filter(office => office.cityId === cityId),
      cityName: city ? city.name : ""
    };
  }

  onContact = office => {
    const { globalRef } = this.props
    const ref = window[globalRef]

    if (!ref.onContactUsClick || typeof(ref.onContactUsClick) !== 'function') {
      alert(`${globalRef}.onContactUsClick" is not bound to any action! Assign it a function that accepts an "Object" as parameters`)
      return;
    }

    ref.onContactUsClick()
  }

  render() {
    const { regionId, cityId, onClose = null } = this.props;
    const { offices, cityName } = this.state;

    return (
      <React.Fragment>
        <div className={`smap__info${!cityId ? ` smap__info--hidden` : ``}`}>
          <h4>{cityName}</h4>
          {offices.map((office, i) => {
            return (
              <React.Fragment key={i} >
                {i > 0 ? <hr /> : null}
                <div className="smap__office">
                  {office.name ? (
                    <p>
                      <strong>{office.name}</strong>
                    </p>
                  ) : null}
                  <Address {...office} />
                  <Contact {...office} />
                  <p>
                    <button className="ux-anchor ux-button button round" onClick={() => this.onContact(office)}>Contact us</button>
                  </p>
                </div>
              </React.Fragment>
            );
          })}
        </div>

        {regionId || cityId ? (
          <button className="smap__detail__close" onClick={() => onClose()} />
        ) : null}
      </React.Fragment>
    );
  }

  componentDidUpdate() {
    const { sourceData, map, cityId } = this.props;

    if (cityId) {
      const ct = sourceData.cities.find(city => city.id === cityId);
      map.panTo({ lat: +ct.lat, lng: +ct.lng });
      map.setZoom(7);
      map.panBy(-200, 0);
    }
  }
}

const Address = ({ address1, address2 }) => {
  const addresses = [address1, address2].join("<br/>");
  if (address1 || address2) {
    return <p dangerouslySetInnerHTML={{ __html: addresses }} />;
  }
};

const Contact = ({ tel, fax }) => {
  if (!tel && !fax) return null

  return (
    <dl>
      {tel ? (
        <React.Fragment>
          <dt>Tel:</dt>
          <dd>{tel}</dd>
        </React.Fragment>
      ) : null}

      {fax ? (
        <React.Fragment>
          <dt>Fax:</dt>
          <dd>{fax}</dd>
        </React.Fragment>
      ) : null}
    </dl>
  );
};

export default InfoWindow;
