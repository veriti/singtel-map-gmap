import React, { Component } from "react";
import ReactDOM from "react-dom";
import GoogleMaps from "./components/GoogleMaps";

import "./App.css";

class App extends Component {
  state = {
    init: false
  }

  componentDidMount() {
    const { globalRef } = this.props
    window[globalRef] = window[globalRef] || {}
    window[globalRef].init = () => this.setState({ init: true })
  }

  render() {
    const { apiKey, sourceData, globalRef, addonsPath } = this.props

    if (!this.state.init) return null

    return ReactDOM.createPortal(
      <GoogleMaps
        id="google-map"
        apiKey={apiKey}
        sourceData={sourceData}
        globalRef={globalRef}
        addonsPath={addonsPath}
      />,
      document.getElementById('mapRoot')
    )
  }
}

export default App;
