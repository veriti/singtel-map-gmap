## Singtel Map Module

### Template Setup

- create a build by running `npm run build`
- the following files will be required to run the module, inject them in your template accordingly
    - `build/static/js/main.[HASH].js`
    - `build/static/css/main.[HASH].css`
    - google-maps-addons.js
- inject the following code snippet, customizing the `data-*` attributes as per your setup. details explained below

```
<div
    id="mapRoot"
    data-api-key="AIzaSyAXbvgRginoznS-jb50Zsv4PGAM34_DArs"
    data-data-source="http://localhost:3001:db"
    data-global-ref="SINGTEL"
></div>
```

### Configuration

- `data-api-key` - google maps api key, you can generate one [here](https://developers.google.com/maps/documentation/javascript/get-api-key)
- `data-data-source` - url for the data source to be used, refer to the format found on the `/src/db.json` file on this repository
- `data-global-ref` - this will be the global variable/namespace to be used to encapsulate the map functions

### Functions and Callbacks

Some functions are exposed over a global variable that will be stored over the namespace you provided on `data-global-ref`.

###### onContactUsClick

Assign a custom function that you'd like to trigger when clicking the `Contact us` button. Function is passed an object from that includes information with regards a certain office.

```
<script>
    // SINGTEL is the namespace you assigned in data-global-ref
    SINGTEL.onContactUsClick = function(object) {
        // console.log(object) // see what information is being thrown by the function
    }
</script>
```

## Requirements

- NodeJS + npm/yarn


## Development

```
npm run develop
or
yarn develop
```

## Building

```
npm run build
or
yarn build
```
